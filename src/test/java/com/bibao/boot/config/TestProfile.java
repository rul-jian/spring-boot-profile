package com.bibao.boot.config;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.model.DBParams;
import com.bibao.boot.springbootprofile.SpringBootProfileApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootProfileApplication.class)
//@ActiveProfiles("dev")
@ActiveProfiles("prod")
public class TestProfile {

	@Autowired
	private DBParams params;
	
	@Test
	public void testDev() {
		assertNotNull(params);
		assertEquals("jdbc:oracle:thin:@BIBAO:1521:XE", params.getUrl());
		assertEquals("oracle.jdbc.driver.OracleDriver", params.getDriver());
		assertEquals("bibao", params.getUsername());
		assertEquals("bibao", params.getPassword());
	}

	@Test
	public void testProd() {
		assertNotNull(params);
		assertEquals("jdbc:microsoft:sqlserver://localhost:1433", params.getUrl());
		assertEquals("com.microsoft.sqlserver.jdbc.SQLServerDriver", params.getDriver());
		assertEquals("dummy", params.getUsername());
		assertEquals("dummy", params.getPassword());
	}
}

package com.bibao.boot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import com.bibao.boot.model.DBParams;

@Configuration
public class DBConfig {
	@Autowired
	private Environment env;
	
	@Profile("dev")
	@Bean
	public DBParams getDevDBParams() {
		DBParams params = new DBParams();
		params.setUrl(env.getProperty("dev.datasource.url"));
		params.setDriver(env.getProperty("dev.datasource.driver.class"));
		params.setUsername(env.getProperty("dev.datasource.username"));
		params.setPassword(env.getProperty("dev.datasource.password"));
		return params;
	}
	
	@Profile("prod")
	@Bean
	public DBParams getProdDBParams() {
		DBParams params = new DBParams();
		params.setUrl(env.getProperty("prod.datasource.url"));
		params.setDriver(env.getProperty("prod.datasource.driver.class"));
		params.setUsername(env.getProperty("prod.datasource.username"));
		params.setPassword(env.getProperty("prod.datasource.password"));
		return params;
	}
}
